import os

from docker import from_env
from pymongo import MongoClient
from importlib import import_module
from functools import wraps

from flask import Flask, redirect, url_for
from flask_restful_swagger_3 import \
    Api, get_swagger_blueprint

from flask_jwt_extended import JWTManager, verify_jwt_in_request


version = int(os.environ.get('API_VERSION', 1))

# Prefix for all urls.
prefix = os.environ.get('API_PREFIX', '/api/v%i') % version

# Prefix for swagget ui and swagger.json pages.
swagger_prefix = prefix + os.environ.get('SWAGGER_PREFIX', '/docs')


# Database.
db = MongoClient(os.environ['DB_URL'])
db = db[os.environ.get('DB_NAME', 'tg-bots-admin-panel')]


# Docker.
docker = from_env()


# Main application.
app = Flask(__name__)


# JWT.
jwt = JWTManager(app)
with open(os.environ['JWT_KEY_FILE']) as file:
    app.config['JWT_SECRET_KEY'] = file.read()


import flask_jwt_extended

# Defalut jwt_required decorator with error handler.
def jwt_required(optional=False, fresh=False, refresh=False, locations=None):
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            try:
                verify_jwt_in_request(optional, fresh, refresh, locations)
            
            except Exception as exc:
                return {
                    'message': str(exc)
                }, 403
            
            return fn(*args, **kwargs)

        return decorator

    return wrapper

# Replace.
flask_jwt_extended.jwt_required = jwt_required

# API.
api = Api(app, version = version,
    authorizations = {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization',
            'scheme': 'bearer',
            'bearerFormat': 'JWT',
            'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token"
        }
    },
    
    add_api_spec_resource = False
)

# Import particular version.
import_module(f'api.v{version}')


# Documentation.
app.config.setdefault('SWAGGER_BLUEPRINT_URL_PREFIX', swagger_prefix)

# App app_context to use SWAGGER_BLUEPRINT_URL_PREFIX variable.
with app.app_context():
    swagger_blueprint = get_swagger_blueprint(api.open_api_object,
        # Already set by SWAGGER_BLUEPRINT_URL_PREFIX.
        swagger_prefix_url = ''
    )

app.register_blueprint(swagger_blueprint, url_prefix = swagger_prefix)

@app.route('/')
@app.route('/index')
def index():
    return redirect(url_for('swagger.show'))
