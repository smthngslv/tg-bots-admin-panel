from api import api, prefix

# Resources.
from . import views

api.add_resource(views.Signin, prefix + '/user/signin')
api.add_resource(views.Signup, prefix + '/user/signup')
api.add_resource(views.Log,    prefix + '/bots/log')
api.add_resource(views.Bot,    prefix + '/bots')
