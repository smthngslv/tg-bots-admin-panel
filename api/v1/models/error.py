from flask_restful_swagger_3 import Schema

from . import String

class Error(Schema):
    type = 'object'
    
    properties = {
        'message': String
    }
    
    required = ['message'] 
 
