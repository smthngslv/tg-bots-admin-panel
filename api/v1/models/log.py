from flask_restful_swagger_3 import Schema

from . import String

class Log(Schema):
    type = 'object'
    
    properties = {
        'stdout': String,
        'stderr': String
    } 
