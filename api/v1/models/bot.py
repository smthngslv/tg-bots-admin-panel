from flask_restful_swagger_3 import Schema

from . import String

class Bot(Schema):
    type = 'object'
    
    properties = {
        'username': String,
        'botname': String,
        'token': String,
        'image': String
    }
    
    required = ['botname', 'token', 'image']
