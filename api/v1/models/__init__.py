from flask_restful_swagger_3 import Schema

class String(Schema):
    type = 'string'


# Store different array schemas.
__array_types = {}

def Array(_type):
    name = f'{_type.__name__}Array'
    
    if name in __array_types:
        return __array_types[name]
    
    cls = type(name, (Schema, ), {
        'type': 'object',
        'properties': {
            'items': _type.array()
        }
    })
    
    __array_types[name] = cls
        
    return cls

from .bot import Bot
from .log import Log
from .error import Error
