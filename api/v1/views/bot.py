import re

from docker.errors import ImageNotFound, APIError, NotFound

from flask_restful.reqparse import RequestParser
from flask_restful_swagger_3 import Resource, swagger
from flask_jwt_extended import current_user, jwt_required

from api import db, docker

from .. import models


@swagger.tags('bot')
class Bot(Resource):
    reqparser = RequestParser()
    reqparser.add_argument('botname', type = str, required = True)
    reqparser.add_argument('image', type = str, 
        help = 'The image in docker.io with bot.')
    reqparser.add_argument('token', type = str, required = True)    
    
    
    @jwt_required()
    @swagger.security(api_key = [])
    @swagger.response(response_code = 201,
        description = 'Successfully added.', 
        no_content = True
    )
    @swagger.response(response_code = 400, 
        description = 'If some error is occured.',
        schema = models.Error
    )
    @swagger.reqparser(name = 'CreateBotRequest', parser = reqparser)
    def post(self):
        if current_user is None:
            return {
                'message': 'User doen not exits.'
            }, 400
    
        args = self.reqparser.parse_args()
        
        botname = args['botname']
        token = args['token']
        image = args['image']
        
        # Check botname.
        if not re.fullmatch(r'([a-zA-Z0-9_\-]){5,32}$', botname):
            return {
                'message': 'The botname does not allow. A-z, 0-9, _ and - symbols only, length between 5 and 32.'
            }, 400
        
        # Bot limit.
        if db.bots.find({'username': current_user['username']}).count() == 5:
            return {
                'message': 'User has too many bots.'
            }, 400
        
        # Try find bot.
        bot = db.bots.find_one(
            {'username': current_user['username'], 'botname': botname})
        
        if not bot is None:
            return {
                'message': 'Bot already exists.'
            }, 400
        
        # Container kwargs.
        kwargs = {
            'name': f"{current_user['username']}_{botname}",
            
            'detach': True,
            
            'environment': {
                'BOT_TOKEN': token
            }
        }
        
        if image is None:
            image = 'smthngslv/telegram-echo-bot'
        
        # Check, if the container already exists.
        try:
            # Get bot container.
            container = \
                docker.containers.get(f"{current_user['username']}_{botname}")
            
            container.remove(force = True)
        
        except NotFound:
            pass
        
        except APIError as exc:
            return {
                'message': str(exc)
            }, 400
        
        try:
            # Create container.
            docker.containers.run(image, **kwargs)
        
        except (ImageNotFound, APIError) as exc:
            return {
                'message': str(exc)
            }, 400
        
        # Add bot.
        db.bots.insert_one({
            'username': current_user['username'], 
            'botname':botname,
            'image': image,
            'token': token
        })
        
        return None, 201
    
    
    @jwt_required()
    @swagger.security(api_key = [])
    @swagger.response(response_code = 200, 
        description = 'Returns the bot information. If botname does not provided returns all bots.',
        schema = models.Array(models.Bot)
    )
    @swagger.response(response_code = 400, 
        description = 'If some error is occured.',
        schema = models.Error
    )
    @swagger.parameters([{
        'in': 'query', 
        'name': 'botname', 
        'schema': models.String
    }])
    def get(self, _parser):
        args = _parser.parse_args()
        
        if args.get('botname'):
            bot = db.bots.find_one({
                'username': current_user['username'], 
                'botname': args['botname']
            }, {'_id': 0, 'username': 0})
        
            if bot is None:
                return {
                    'message': 'Bot does not exist.' 
                }, 400
            
            return {
                'items': [bot, ]
            }, 200
          
        bots = db.bots.find(
            {'username': current_user['username']}, {'_id': 0, 'username': 0})
          
        return {
            'items': [bot for bot in bots]
        }, 200
    
    
    @jwt_required()
    @swagger.security(api_key = [])
    @swagger.response(response_code = 200,
        description = 'Successfully deleted.', 
        no_content = True
    )
    @swagger.response(response_code = 400, 
        description = 'If some error is occured.',
        schema = models.Error
    )
    @swagger.parameters([{
        'in': 'query', 
        'name': 'botname', 
        'schema': models.String,
        'required': True
    }])
    def delete(self, _parser):
        args = _parser.parse_args()
        
        # Find bot.
        bot = db.bots.find_one({
            'username': current_user['username'],
            'botname': args['botname']
        })
        
        if bot is None:
            return {
                'message': 'Bot or user does not exist.' 
            }, 400
        
        try:
            # Get bot container.
            container = \
                docker.containers.get(f"{bot['username']}_{bot['botname']}")
            
            container.remove(force = True)
        
        except NotFound:
            pass
            
        except APIError as exc:
            return {
                'message': str(exc)
            }, 400
        
        db.bots.delete_one({
            'username': bot['username'],
            'botname': bot['botname']
        })
        
        return None, 200
