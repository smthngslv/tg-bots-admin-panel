import os
import re
import hashlib

from flask_restful.reqparse import RequestParser
from flask_restful_swagger_3 import Resource, swagger

from api import db

from .. import models

class Signup(Resource):
    reqparser = RequestParser()
    reqparser.add_argument('username', type = str, required = True)
    reqparser.add_argument('password', type = str, required = True)
    
    @swagger.tags('signup')
    @swagger.response(response_code = 201,
        description = 'Successfully logged in.', 
        no_content = True
    )
    @swagger.response(response_code = 400, 
        description = 'If some error is occured.',
        schema = models.Error
    )
    @swagger.reqparser(name = 'SignupRequest', parser = reqparser)
    def post(self):
        args = self.reqparser.parse_args()
        
        username = args['username']
        password = args['password']
        
        # Already exists.
        if db.users.find_one({'username': username}):
            return {
                'message': 'User already exists.'
            }, 400
        
        # Check username.
        if not re.fullmatch(r'([a-zA-Z0-9_]){5,32}$', username):
            return {
                'message': 'The username does not allow. A-z, 0-9 and _ symbols only, length between 5 and 32.'
            }, 400
        
        # Check password.
        if not re.fullmatch(r'.{8,64}$', password):
            return {
                'message': 'The password does not allow. Any symbols, the length between 8 and 64.'
            }, 400
        
        # Generate salt and key.
        salt = os.urandom(32)
        key = hashlib.pbkdf2_hmac(
            'sha256', password.encode('utf-8'), salt, 100000)
        
        # Add user.
        db.users.insert_one({
            'username': username, 
            
            'security': {
                'salt': salt,
                'key': key
            }
        })
        
        return None, 201
