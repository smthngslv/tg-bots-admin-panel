import hashlib

from flask_restful.reqparse import RequestParser
from flask_restful_swagger_3 import Resource, Schema, swagger
from flask_jwt_extended import create_access_token

from api import jwt, db

from .. import models

@jwt.user_identity_loader
def user_identity_lookup(username):
    return username

@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    
    # Load the user.
    return db.users.find_one({'username': identity}, {'_id': 0})

class SigninResponce(Schema):
    type = 'object'
    
    properties = {
        'token': models.String
    }
    
    required = ['token'] 

class Signin(Resource):    
    reqparser = RequestParser()
    reqparser.add_argument('username', type = str, required = True)
    reqparser.add_argument('password', type = str, required = True)
    
    @swagger.tags('signin')
    @swagger.response(response_code = 200,
        description = 'Returns an access token.',
        schema = SigninResponce
    )
    @swagger.response(response_code = 400, 
        description = 'If some error is occured.',
        schema = models.Error
    )
    @swagger.reqparser(name = 'SigninRequest', parser = reqparser)
    def post(self):
        args = self.reqparser.parse_args()
        
        username = args['username']
        password = args['password']
        
        user = db.users.find_one({'username': username}, {'security': 1})
        
        # User not found.
        if user is None:
            return {
                'message': 'Bad username or password.'
            }, 400
        
        # Compute key.
        key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'),\
            user['security']['salt'], 100000)
        
        # Check.
        if user['security']['key'] != key:
            return {
                'message': 'Bad username or password.'
            }, 400
        
        return {
            'token': create_access_token(identity = username)
        }, 200
