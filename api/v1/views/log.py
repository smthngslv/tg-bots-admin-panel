import re

from docker.errors import ImageNotFound, APIError, NotFound

from flask_restful.reqparse import RequestParser
from flask_restful_swagger_3 import Resource, swagger
from flask_jwt_extended import current_user, jwt_required

from api import db, docker

from .. import models

class Log(Resource):
    @jwt_required()
    @swagger.tags('log')
    @swagger.security(api_key = [])
    @swagger.response(response_code = 200, 
        description = 'Returns the log of the bot container.',
        schema = models.Log
    )
    @swagger.response(response_code = 400, 
        description = 'If some error is occured.',
        schema = models.Error
    )
    @swagger.parameters([{
        'in': 'query', 
        'name': 'botname', 
        'schema': models.String,
        'required': True
    }])
    def get(self, _parser):
        args = _parser.parse_args()
        
        bot = db.bots.find_one({
            'username': current_user['username'], 
            'botname': args['botname']
        })
    
        if bot is None:
            return {
                'message': 'Bot does not exist.' 
            }, 400
        
        try:
            # Get bot container.
            container = \
                docker.containers.get(f"{bot['username']}_{bot['botname']}")
         
        except (APIError, NotFound) as exc:
            return {
                'message': str(exc)
            }, 400
        
        # Check logs.
        out = container.logs(stderr = False).decode()
        err = container.logs(stdout = False).decode()
        
        return {
            'stdout': out,
            'stderr': err
        }, 200
