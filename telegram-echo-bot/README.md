## Telegram Echo Bot

A Simple telegram bot. To use the bot provide `BOT_TOKEN` environment variable.

Taken from https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/echobot.py
