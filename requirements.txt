flask >= 1.1.2
flask-jwt-extended >= 4.1.0
flask-restful-swagger-3 >= 0.4.6
docker >= 5.0.0
pymongo >= 3.11.3
python-dotenv >= 0.17.0
