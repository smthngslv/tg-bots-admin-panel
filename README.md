Telegram Bots Admin Panel
===

[[_TOC_]]

## Local running
To run the app locally you need to install `docker`, `python 3.8+` and run `mongodb` instance.

1. Configure `.env` file. 
```bash=
cp .env.example .env
```

2. Set your `mongodb` address. If you use remote docker daemon, set `DOKER_HOST` variable:
```bash=
# The starting up, does not need to change. Only for local testing.
FLASK_APP=api

# Address of mongo db instance.
DB_URL=mongodb://localhost:27017/

# Path to the file with JWT key.
JWT_KEY_FILE=jwt_secret.key

# Docker daemon address.
#DOCKER_HOST=tcp://localhost:2375

# Optional database name.
DB_NAME=tg-bots-admin-panel

# Optional debug options. Works local only.
FLASK_ENV=development
FLASK_RUN_HOST=0.0.0.0
FLASK_RUN_PORT=5000

# Optional API configuration.
API_VERSION=1
API_PREFIX=/api/v%i

# Optional. Documentation.
SWAGGER_PREFIX=/docs
```

3. Create environment and install requirements:
```bash=
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

4. To start the app use command (`sudo` to be able to connect too docker ipc):
```bash=
sudo flask run
```

## Use
After starting the server, `swagger` documentation can be found on `/` of the site. Firsly, it is nessasary to create a user with `/user/signup` method. To get access token use `/user/signin` method. After that, you will be able to use the auth in top left corner of the `swagger` page. To add a bot use `/bot` method.

**`POST` request on `/bot` method without `image` parameter will add a simple echo bot.**


Via curl:

1. Signup.
```bash=
curl -X POST "http://localhost:5000/api/v1/user/signup" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"username\":\"admin\",\"password\":\"adminpass\"}"
```

2. Signin.
```bash=
curl -X POST "http://localhost:5000/api/v1/user/signin" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"username\":\"admin\",\"password\":\"adminpass\"}"
```

3. Add bot.
```bash=
curl -X POST "http://localhost:5000/api/v1/bots" -H "accept: */*" -H "Authorization: Bearer <TOKEN>" -H "Content-Type: application/json" -d "{\"botname\":\"my_bot\",\"token\":\"<BOT_TOKEN>\"}"
```

4. Since each bot - is a docker container, you can check logs with:
```bash=
curl -X GET "http://localhost:5000/api/v1/bots/log?botname=my_bot" -H "accept: application/json" -H "Authorization: Bearer <TOKEN>"
```
